package com.bvrit.hospitalmanagement.bean;

public class GlobOrthBean1 {

	String name;
	String email;
	String phone;
	String age;
	String date;
	String time;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public GlobOrthBean1(String name, String email, String phone, String age,
			String date, String time) {
		super();
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.age = age;
		this.date = date;
		this.time = time;
	}
	public GlobOrthBean1() {
		super();
	}
	
	
	
}
