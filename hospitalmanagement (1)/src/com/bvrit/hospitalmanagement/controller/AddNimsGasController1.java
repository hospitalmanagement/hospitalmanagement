package com.bvrit.hospitalmanagement.controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bvrit.hospitalmanagement.bean.NimsGasBean1;
import com.bvrit.hospitalmanagement.dao.NimsGasDAO1;


public class AddNimsGasController1 extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException {
		try {
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			String phone = request.getParameter("phone");
			String age = request.getParameter("age");
			String date = request.getParameter("date");
			String time = request.getParameter("time");
			
			NimsGasBean1 dBean = new NimsGasBean1(name,email,phone,age,date,time);
			NimsGasDAO1 ddao = new NimsGasDAO1();
			int result = ddao.addDeduction(dBean);
			if(result >= 1){
				response.sendRedirect("ViewNimsGas1.jsp");
			}
			else{
				System.out.println("Record Not Inserted");
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
