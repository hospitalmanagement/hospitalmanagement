package com.bvrit.hospitalmanagement.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.hospitalmanagement.bean.GlobGyneBean1;
import com.bvrit.hospitalmanagement.dao.GlobGyneDAO1;

public class DeleteGlobGyneController1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteGlobGyneController1() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession hs = request.getSession();
		String date = request.getParameter("date");
		int result;	
			try {
				GlobGyneDAO1 cdao = null;
				try {
					cdao = new GlobGyneDAO1();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				result = cdao.delete(date);
				if(result >= 0){
					response.sendRedirect("ViewGlobGyne1.jsp");
				}else{
					response.sendRedirect("ViewGlobGyne1.jsp");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
