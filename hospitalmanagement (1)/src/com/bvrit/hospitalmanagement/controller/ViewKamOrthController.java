package com.bvrit.hospitalmanagement.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bvrit.hospitalmanagement.bean.KamOrthBean;
import com.bvrit.hospitalmanagement.dao.KamOrthDAO;
public class ViewKamOrthController extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
    protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    	try {
    		String name = request.getParameter("name");
			String email = request.getParameter("email");
			String phone = request.getParameter("phone");
			String age = request.getParameter("age");
			String date = request.getParameter("date");
			String time = request.getParameter("time");
			
			KamOrthBean dBean = new KamOrthBean(name,email,phone,age,date,time);
			KamOrthDAO dDao = new KamOrthDAO();
			ResultSet rs = (ResultSet) dDao.viewDeduction();
			if(((ResultSet) rs).next()){
				response.sendRedirect("ViewKamOrth.jsp");
			}
			else{
				response.sendRedirect("AddKamOrth.jsp");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	
	      
	}
}
