package com.bvrit.hospitalmanagement.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bvrit.hospitalmanagement.bean.KamOrthBean;
import com.bvrit.hospitalmanagement.dao.KamOrthDAO;

public class DeleteKamOrthController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteKamOrthController() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession hs = request.getSession();
		String date = request.getParameter("date");
		int result;	
			try {
				KamOrthDAO cdao = null;
				try {
					cdao = new KamOrthDAO();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				result = cdao.delete(date);
				if(result >= 0){
					response.sendRedirect("ViewKamOrth.jsp");
				}else{
					response.sendRedirect("ViewKamOrth.jsp");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
