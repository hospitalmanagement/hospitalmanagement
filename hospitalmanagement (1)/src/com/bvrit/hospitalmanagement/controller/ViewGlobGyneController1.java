package com.bvrit.hospitalmanagement.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bvrit.hospitalmanagement.bean.GlobGyneBean1;
import com.bvrit.hospitalmanagement.dao.GlobGyneDAO1;
public class ViewGlobGyneController1 extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
    protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    	try {
    		String name = request.getParameter("name");
			String email = request.getParameter("email");
			String phone = request.getParameter("phone");
			String age = request.getParameter("age");
			String date = request.getParameter("date");
			String time = request.getParameter("time");
			
			GlobGyneBean1 dBean = new GlobGyneBean1(name,email,phone,age,date,time);
			GlobGyneDAO1 dDao = new GlobGyneDAO1();
			ResultSet rs = (ResultSet) dDao.viewDeduction();
			if(((ResultSet) rs).next()){
				response.sendRedirect("ViewGlobGyne1.jsp");
			}
			else{
				response.sendRedirect("AddGlobGyne1.jsp");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	
	      
	}
}
