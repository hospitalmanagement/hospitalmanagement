package com.bvrit.hospitalmanagement.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bvrit.hospitalmanagement.bean.NimsCardBean;
import com.bvrit.hospitalmanagement.dao.NimsCardDAO;
public class ViewNimsCardController extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
    protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    	try {
    		String name = request.getParameter("name");
			String email = request.getParameter("email");
			String phone = request.getParameter("phone");
			String age = request.getParameter("age");
			String date = request.getParameter("date");
			String time = request.getParameter("time");
			
			NimsCardBean dBean = new NimsCardBean(name,email,phone,age,date,time);
			NimsCardDAO dDao = new NimsCardDAO();
			ResultSet rs = (ResultSet) dDao.viewDeduction();
			if(((ResultSet) rs).next()){
				response.sendRedirect("ViewNimsCard.jsp");
			}
			else{
				response.sendRedirect("AddNimsCard.jsp");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	
	      
	}
}
