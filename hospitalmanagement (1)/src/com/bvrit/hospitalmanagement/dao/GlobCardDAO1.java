package com.bvrit.hospitalmanagement.dao;
import java.sql.Connection;
import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.bvrit.hospitalmanagement.bean.GlobCardBean1;

public class GlobCardDAO1 {
	Connection conn;
	ConnectionDAO cdao;
	PreparedStatement pst;
	public GlobCardDAO1() throws ClassNotFoundException{
		cdao = new ConnectionDAO();
		conn = cdao.getConnection();
	}
	public int addDeduction(GlobCardBean1 dBean) throws SQLException{
		String name=dBean.getName();
		String email=dBean.getEmail();
	    String phone = dBean.getPhone();
        String age=dBean.getAge();
		String date=dBean.getDate();
		String time=dBean.getTime();
		
		String query = "insert into globcard1 values(?,?,?,?,?,?)";
		int result = 0;
		pst = conn.prepareStatement(query);
		pst.setString(1, name);
		pst.setString(2, email);
		pst.setString(3, phone);
	    pst.setString(4, age);
		pst.setString(5, date);
		pst.setString(6, time);
		result = pst.executeUpdate();
		return result;
		
	}
	public List<GlobCardBean1> viewDeduction() throws SQLException{
	    //JTtable result = new JTable(data, columnNames);
	    ResultSet result = null;
	    List<GlobCardBean1> list = new ArrayList<GlobCardBean1>();
	    GlobCardBean1 db;
	    String query = "SELECT * FROM globcard1";// WHERE DED_CODE='" + DED_CODE +"'";
	    pst = conn.prepareStatement(query);
	    result = (pst.executeQuery());
	    while(result.next()){
	        db = new GlobCardBean1();
	        //System.out.println(result.getString("name"));
	        db.setName(result.getString("name"));
	        db.setEmail(result.getString("email"));
	        db.setPhone(result.getString("phone"));
	        db.setAge(result.getString("age"));
	        db.setDate(result.getString("date"));
	        db.setTime(result.getString("time"));
	        
	        list.add(db);
	    }
	   
	   
	    return list;
}
	
	public int delete(String date) throws SQLException{
		
		int result;
		String query = "DELETE FROM globcard1 WHERE date ='" + date +"'";
		pst = conn.prepareStatement(query);
		result = pst.executeUpdate();
		pst.close();
		return result;
		
	}
}
	


