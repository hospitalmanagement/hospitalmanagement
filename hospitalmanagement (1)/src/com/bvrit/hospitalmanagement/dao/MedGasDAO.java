package com.bvrit.hospitalmanagement.dao;
import java.sql.Connection;
import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.bvrit.hospitalmanagement.bean.MedGasBean;

public class MedGasDAO {
	Connection conn;
	ConnectionDAO cdao;
	PreparedStatement pst;
	public MedGasDAO() throws ClassNotFoundException{
		cdao = new ConnectionDAO();
		conn = cdao.getConnection();
	}
	public int addDeduction(MedGasBean dBean) throws SQLException{
		String name=dBean.getName();
		String email=dBean.getEmail();
	    String phone = dBean.getPhone();
        String age=dBean.getAge();
		String date=dBean.getDate();
		String time=dBean.getTime();
		
		String query = "insert into medgas values(?,?,?,?,?,?)";
		int result = 0;
		pst = conn.prepareStatement(query);
		pst.setString(1, name);
		pst.setString(2, email);
		pst.setString(3, phone);
	    pst.setString(4, age);
		pst.setString(5, date);
		pst.setString(6, time);
		result = pst.executeUpdate();
		return result;
		
	}
	public List<MedGasBean> viewDeduction() throws SQLException{
	    //JTtable result = new JTable(data, columnNames);
	    ResultSet result = null;
	    List<MedGasBean> list = new ArrayList<MedGasBean>();
	    MedGasBean db;
	    String query = "SELECT * FROM medgas";// WHERE DED_CODE='" + DED_CODE +"'";
	    pst = conn.prepareStatement(query);
	    result = (pst.executeQuery());
	    while(result.next()){
	        db = new MedGasBean();
	        //System.out.println(result.getString("name"));
	        db.setName(result.getString("name"));
	        db.setEmail(result.getString("email"));
	        db.setPhone(result.getString("phone"));
	        db.setAge(result.getString("age"));
	        db.setDate(result.getString("date"));
	        db.setTime(result.getString("time"));
	        
	        list.add(db);
	    }
	   
	   
	    return list;
}
	
	public int delete(String date) throws SQLException{
		
		int result;
		String query = "DELETE FROM medgas WHERE date ='" + date +"'";
		pst = conn.prepareStatement(query);
		result = pst.executeUpdate();
		pst.close();
		return result;
		
	}
}
	


