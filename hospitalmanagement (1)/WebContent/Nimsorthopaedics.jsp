<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <style type="text/css">
 .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 1890px;
      height:400px;
      margin: auto;
  }
.tab{
width: 1140px;
 margin: auto;
}
.doc{
margin-top:30px;
margin-left:100px;
}
  </style>
<title>Cardiology</title>
</head>
<body>
<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="./IMG/unnamed.jpg" width="460" height="345">
      </div>

      
    
      <div class="item">
        <img src="./IMG/h1.JPG" width="460" height="345">
      </div>

      <div class="item">
        <img src="./IMG/h3.jpg" width="460" height="345">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div class="tab">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Hospital Management</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="list.jsp">List of Hospitals</a></li>
      </ul>
      <ul class="nav navbar-nav">
        <li class="active"><a href="Nims.jsp">Nims</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="LogoutController"><span class="glyphicon glyphicon-user"></span> Sign Out</a></li>
       
      </ul>
    </div>
  </div>
</nav>
</div>
<div class="doc">
<h4 align="center">NIMS-Orthopaedics</h4>
<br>
<br>DR. ANANDAN NARAYANASWAMY<br> MBBS, D ORTHO,MS ORTHO<br> Specially trained in Arthroscopy<br>
Hyderabad,Panjagutta
</b><br>
<br>
<a href="AddNimsOrth.jsp" class="btn btn-info" role="button">Make Appointment</a>
<br>
<br>
<br>
<b>DR. NIRANJAN.T.J, D (ORTHO)<br> DNB(ORTHO) MNAMS<br> Fellowship in Arthroplasty<br>
Hyderabad,Panjagutta
</b>
<br>
<br>
<a href="AddNimsOrth1.jsp" class="btn btn-info" role="button">Make Appointment</a>
<br>
<br>
</div>
</body>
</html>