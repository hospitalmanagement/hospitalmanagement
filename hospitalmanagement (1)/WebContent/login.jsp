<%
String unm = request.getParameter("username");
String error = request.getParameter("error");
if(unm != null)
{
   out.print("you are not valid user"+unm);
}
if(error != null)
{
   out.print("<h1><font color = red>"+ error +"</font></h1>");
}
%>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
<title>Login</title>
<style>
.carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 1890px;
      height:400px;
      margin: auto;
  }
.tab{
width: 1140px;
 margin: auto;
}
.container1{
 background: url(./IMG/1g.jpg) no-repeat center center fixed;
      width: 1138px;
      height:600px;
      margin: auto; 
}
.panel-default {
opacity: 0.9;
margin-top:100px;
margin-left:300px;
}
.form-group.last { margin-bottom:0px; }
.wrapper {	
	width:500px;
	height:500px;
	margin:auto;
}

.form-signin {
  max-width: 500px;
  padding: 15px 35px 45px;
  margin: 0 auto;
  background-color: #fff;
  border: 1px solid rgba(0,0,0,0.1);
  }  
  .input[type="text"] {
	  margin-bottom: -1px;
	  border-bottom-left-radius: 0;
	  border-bottom-right-radius: 0;
	}

	input[type="password"] {
	  margin-bottom: 20px;
	  border-top-left-radius: 0;
	  border-top-right-radius: 0;
	}
	.image { 
   position: relative; 
   left: 75px;
   height:300px;
}

  
</style>
</head>

<body>
<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="./IMG/unnamed.jpg" width="460" height="345">
      </div>

      
    
      <div class="item">
        <img src="./IMG/1l.jpg" width="460" height="345">
      </div>

      <div class="item">
        <img src="./IMG/1a.jpg" width="460" height="345">
      </div>
      
      <div class="item">
        <img src="./IMG/1d.jpg" width="460" height="345">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div class="tab">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Hospital Management</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="HospitalManagement.html">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="signup.jsp"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
       
      </ul>
    </div>
  </div>
</nav>
</div>
<div class="container1">
    <div class="row" >
        <div class="col-sm-9" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-lock"></span> Login</div>
                <div class="panel-body">
                    <form class="form-horizontal"  action="LoginController"  >
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">
                            UserName</label>
                        <div class="col-sm-9">
                            <input type="text" name = "user" class="form-control" id="inputEmail3" placeholder="username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">
                            Password</label>
                        <div class="col-sm-9">
                            <input type="password"  name = "password" class="form-control" id="inputPassword3" placeholder="Password" required>
                        </div>
                    </div>
                    
                    
                        <div class="form-group last">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-success btn-sm">
                                Sign in</button>
    </div>
    </div>
    



                           </form>      
                        </div>
                    </div>
                   
                </div>
                </div>
        </div>


</body>
</html>