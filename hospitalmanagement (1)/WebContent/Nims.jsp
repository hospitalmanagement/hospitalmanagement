<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html>
	<head>
		<title>HospitalManagement</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Play-Offs Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() {setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!meta charset utf="8">
		<!--fonts-->
			<link href='http://fonts.googleapis.com/css?family=Monda:400,700' rel='stylesheet' type='text/css'>
			<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700' rel='stylesheet' type='text/css'>
		<!--fonts-->
		<!--owlcss-->
		<link href="css/owl.carousel.css" rel="stylesheet">
		<!--bootstrap-->
			<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<!--coustom css-->
			<link href="css/style.css" rel="stylesheet" type="text/css"/>
		<!--default-js-->
			<script src="js/jquery-2.1.4.min.js"></script>
		<!--bootstrap-js-->
			<script src="js/bootstrap.min.js"></script>
		<!--script-->
			<script type="text/javascript" src="js/move-top.js"></script>
			<script type="text/javascript" src="js/easing.js"></script>
		<!--script-->
	</head>
	<body>
		<div class="header" id="home">
			<div class="header-top">
				<div class="container" align="center">
					<p>NIMS</p>
				</div>
			</div>
			<div class="header_nav" id="home">
				<nav class="navbar navbar-default chn-gd">
					<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand logo-st" href="HospitalManagement.html">HospitalManagement</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
						<a href="list.jsp" >
						<span class="glyphicon glyphicon-home icn_pos hm" aria-hidden="true"></span><br>
						Home
						</a>
						</li>
						<!---->
						<li>
						<a href="#features" class="scroll">
						<span class="glyphicon glyphicon-cog icn_pos" aria-hidden="true"></span><br>
						Services
						</a>
						</li>
						<!---->
						<li>
						<a href="#acheive" class="scroll">
						<span class="glyphicon glyphicon-thumbs-up icn_pos hm2" aria-hidden="true"></span><br>
						Achivements
						</a>
						</li>
						<!---->
						<!--script-->
						<script type="text/javascript">
						jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
						});
						});
						</script>
						<!--script-->
					</ul>
					</div><!-- /.navbar-collapse -->
					<div class="clearfix"></div>
					</div><!-- /.container-fluid -->
				</nav>
			</div>
			<div class="header_banner">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
					<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
					</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
				<div class="item  image-wid">
					<img src="./IMG/1d.jpg" alt="..." class="img-responsive">
					<div class="carousel-caption">
						<h3>Drugs  For Required Dose</h3>
						<p>The dose is the amount of drug taken at any one time. This can be expressed as the weight of drug,volume of drug,the number of dosage forms, or some other quantity.
						The total daily dose is calculated from the dose and the number of times per day the dose is taken.</p>
						
					</div>
					</div>
					<div class="item active  image-wid">
					<img src="./IMG/1a.jpg" alt="..." class="img-responsive">
					<div class="carousel-caption">
						<h3>Medical Check Up Instruments</h3>
						<p>Medical instrument - instrument used in the practice of medicine.<br>
Ballistocardiograph, Cardiograph,Bronchoscope,Cardiograph, Electrocardiograph,Dialyzer,Diathermy machine,
Electroencephalograph, Electromyograph ,Endoscope ,Fiberscope.</p>
						
					</div>
					</div>
					
					<div class="item  image-wid">
					<img src="./IMG/1g.jpg" alt="..." class="img-responsive">
					<div class="carousel-caption">
						<h3>Doctors Supervision</h3>
						<p>A medical supervisor,will monitor your progress. The progress that you are making under treatment, based on my observations and discussions with your doctor(s) and others.

These discussions will include details of any improvements in your health, your current treatment, your prognosis and results of any testing carried out by your doctor or at your workplace.</p>
						
					</div>
					</div>
					<div class="item  image-wid">
					<img src="./IMG/1l.jpg" alt="..." class="img-responsive">
					<div class="carousel-caption">
						<h3>Viral Treatments</h3>
						<p>Bacterial and viral infections have many things in common. Both types of infections are caused by microbes -- bacteria and viruses, respectively. </p>
						
					</div>
					</div>
				</div>
				<!-- Controls -->
				<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
				</a>
				</div>
			</div>
		</div>
		
					<div class="style-label">
			<div class="container">
				<ul class="box-shadow effect2">
					<li class="col-md-3">
						<span class="glyphicon glyphicon-leaf flt" aria-hidden="true"></span>
						<div class="label-text">
						 <h3>Clean</h3>
                        <p>The surroundings are clean and hygienic which makes it free from diseases.</p>
                        </div>
                    </li>
                    <li class="col-md-3">
                        <span class="glyphicon glyphicon-eye-open flt" aria-hidden="true"></span>
                        <div class="label-text">
                        <h3>Focusing</h3>
                        <p>The focus is maintained and are service is whole hearted</p>
                        </div>
                    </li>
                    <li class="col-md-3">
                        <span class="glyphicon glyphicon-pencil flt" aria-hidden="true"></span>
                        <div class="label-text">
                        <h3>Prescription</h3>
                        <p> Prescriptions may include orders to be performed by a patient, caretaker, nurse, pharmacist, physician, other therapist. </p>
                        </div>
                    </li>
                    <li class="col-md-3">
                        <span class="glyphicon glyphicon-cutlery flt" aria-hidden="true"></span>
                        <div class="label-text">
                        <h3>Diet</h3>
                        <p>Dietary habits and choices play a significant role in the quality of life, health and longevity.</p>
                        </div>
                    </li>
        
                </ul>
            </div>
        </div>
		<div class="content">
			<div class="service_features" id="features">
				<div class="container">
					<div class="col-md-4 ser-fet">
						<h3>Our Services</h3>
						<p>We are good at</p>
						<span class="line"></span>
						<div class="services">
							<div class="menu-grid">
								
									
									<a href="Nimscardiology.jsp" class="active">Heart specialist</a>
										<ul>
											<li class="subitem1">
												<p> As the center focus of cardiology, the heart has numerous anatomical features and numerous physiological features.</p>
											</li>
										</ul>
									
									<a href="Nimsgastroenterology.jsp" class="active">Gastroentrology</a>
										<ul>
											<li class="subitem1">
												<p>Gastroentrology is concerned with the disorders of the stomach and intestines</p>
											</li>
										</ul>
									
									<a href="Nimsorthopaedics.jsp" class="active">Orthopedic</a>
										<ul>
											<li class="subitem1">
												<p>orthopaedics is concerned with conditions involving the musculoskeletal system.</p>
											</li>
										</ul>
									
								    
									
									
									
									
								
								<!-- script for tabs -->
								<script type="text/javascript">
									$(function() {
										var menu_ul = $('.menu_drop > li > ul'),
											menu_a  = $('.menu_drop > li > a');
												menu_ul.hide();
													menu_a.click(function(e) {
													e.preventDefault();
													if(!$(this).hasClass('active')) {
													menu_a.removeClass('active');
													menu_ul.filter(':visible').slideUp('normal');
													$(this).addClass('active').next().stop(true,true).slideDown('normal');
													} else {
													$(this).removeClass('active');
												$(this).next().stop(true,true).slideUp('normal');
											}
										});
									});
								</script>
							<!-- script for tabs -->
						</div>
						</div>
					</div>
					<div class="col-md-8 ser-fet">
						<h3>Our Features</h3>
						<p>Our Aim</p>
						<span class="line"></span>
						<div class="features">
							<div class="col-md-6 fet-pad">
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-user aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Patient Profile</h4>
										<p>The data is gathered on an individual subject who participates in a clinical trial.</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-screenshot aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Advanced Equipment</h4>
										<p> Equipment includes medical imaging machines,ultrasound,MRI machines, medical lasers ,LASIK surgical machines.</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-ok aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Operations sucessed</h4>
										<p>There are many successfull operations done under our supervision with better utilization of advanced equipment. </p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-6 fet-pad">
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-piggy-bank aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Health Insurance</h4>
										<p>We provide you with Health insurence it protects you from paying the full costs of medical services when you're injured or sick.</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-education aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Recognised Doctors</h4>
										<p>We have the best qualified team of doctor with great reputation.</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-heart aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Satisfaction</h4>
										<p>The satisfaction of the patient is our primary concern.</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		<div class="acheivments" id="acheive">
			<div class="container top">
					<h3>Acheivments</h3>
					<!--script-->
					<link rel="stylesheet" href="css/swipebox.css">
					<script src="js/jquery.swipebox.min.js"></script>
					<script type="text/javascript">
								jQuery(function($) {
									$(".swipebox").swipebox();
								});
					</script>
					<!--script-->
					<div class="gallery-grids">
						<ul>
							<li class="col-md-4 gal-alt">
								<a href="IMG/1i.jpg" class="swipebox">
								<img src="IMG/1i.jpg" alt="/"/>
									<span class="hide-box">
										<h4>Neutron collider Microscope</h4>
										<p>Great transparency Rate</p>
									</span>
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="IMG/1p.jpg" class="swipebox">
								<img src="IMG/1p.jpg" alt="/"/>
									<span class="hide-box">
										<h4>German made Ak-2000 Ct-Scan</h4>
										<p>They deliver much more radiation than X-rays. </p>
									</span>
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="IMG/1n.jpg" class="swipebox">
								<img src="IMG/1n.jpg" alt="/" />
								<span class="hide-box">
										<h4>Symbiosis Laser T-300</h4>
										<p>A laser produces a narrow beam of light that is intense enough to cut through tissue.</p>
								</span>
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="IMG/1k.jpg" class="swipebox">
								<img src="IMG/1k.jpg" alt="/" />
								<span class="hide-box">
										<h4>Angeo Blast</h4>
										<p>Angioblasts are one of the two products formed from hemangioblasts .</p>
								</span>	
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="IMG/1o.jpg" class="swipebox">
								<img src="IMG/1o.jpg" alt="/" />
								<span class="hide-box">
										<h4>Spacious Operation Theater</h4>
										<p>The Operation theater with best equipment is provided. </p>
								</span>
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="IMG/1f.jpg" class="swipebox">
								<img src="IMG/1f.jpg" alt="/"/>
								<span class="hide-box">
										<h4>Latest Ventilators</h4>
										<p>The ventilators from equipment from medical brands such as Drager, Newport, Pulmoneticare provided.</p>
								</span>
							</li></a>
							<div class="clearfix"></div>
						</ul>
					</div>
			</div>
		</div>
		</div>
		
			<div class="contact-form" align="center">
				<div class="container">
					<div class="col-md-3 pd adress">
						<h3>adress</h3>
						<ul>
					<li>		Punjagutta,</li>
<li>Hyderabad</li> 
<li>Telangana, India</li>
<li>Tel: (91-40) 23489000</li>
<li>Fax: (91-40) 23310076</li>
							
						</ul>
					</div>
					</div></div>
		<div class="footer">
			<div class="container">
				<div class="footer-text">
				<h3><a href="#">HospitalManagement</a></h3>
				
				</div>
			</div>
		</div>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</body>
</html>