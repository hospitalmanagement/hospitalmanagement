<%@page import="com.bvrit.hospitalmanagement.dao.KamUroDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.util.List" %>
<%@ page import = "com.bvrit.hospitalmanagement.dao.KamUroDAO" %>
<%@ page import = "com.bvrit.hospitalmanagement.dao.ConnectionDAO" %>
<%@ page import = "com.bvrit.hospitalmanagement.bean.KamUroBean" %>
<%
KamUroBean cb = new KamUroBean();
        HttpSession hs = request.getSession();
        KamUroDAO ddao = new KamUroDAO();
        List<KamUroBean> list = ddao.viewDeduction();
        ListIterator<KamUroBean> itr = list.listIterator();
%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<title>Appointment Booking</title>
</head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
   <div class="navbar-header">
      <a class="navbar-brand" href="#">Hospital Management</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="list.jsp">List of Hospitals</a></li>
      </ul>
      
      <ul class="nav navbar-nav">
        <li class="active"><a href="KamUro.jsp">Book My Doctor</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="LogoutController"><span class="glyphicon glyphicon-user"></span> Sign Out</a></li>
       
      </ul>
    </div>
  </div>
</nav>

<form action = "ViewKamUroController">
<h1  align="center">Appointment</h1>
<table border = 2 align="center">
<tr>
<th>Name</th>
<th>Email</th>
<th>Phone number</th>
<th>Age</th>
<th>Date</th>
<th>Time</th>
<th>DELETE</th>

</tr>
<%
    for(KamUroBean cbs: list){
        out.print("<tr>");
        out.print("<td>" + cbs.getName()+"</td>");
        out.print("<td>" + cbs.getEmail()+"</td>");
        out.print("<td>" + cbs.getPhone()+"</td>");
        out.print("<td>" + cbs.getAge()+"</td>");
        out.print("<td>" + cbs.getDate()+"</td>");
        out.print("<td>" + cbs.getTime()+"</td>");
        out.print("<td><a href = 'DeleteKamUroController?date=" + cbs.getDate() +"'><img src =./IMG/delete.png width = 90 height = 90></img></a></td>");
       // out.print("<td><a href = 'DeleteTrain1?journeydate=" + cbs.getJourneydate() +"'><img src =./IMG/delete.png width = 90 height = 90></img></a></td>");
        //out.print("<td><a href = 'UpdateDeduction.jsp?DED_CODE=" + cbs.getDED_CODE() +"&DED_DESC=" + cbs.getDED_DESC()+"&DED_START_DATE=" + cbs.getDED_START_DATE()+"&DED_END_DATE=" + cbs.getDED_END_DATE()+"&DEDTAXABILITY=" + cbs.getDED_TAXABILITY()+"&DED_DFLT_AMOUNT=" + cbs.getDED_DFLT_AMOUNT()+"'><img src =./IMG/update.jpg width = 90 height = 90></img></a></td>");
        //out.print("<td><a href = 'AddDeduction.jsp?DED_CODE=" + cbs.getDED_CODE() +"&DED_DESC=" + cbs.getDED_DESC()+"&DED_START_DATE=" + cbs.getDED_START_DATE()+"&DED_END_DATE=" + cbs.getDED_END_DATE()+"&DEDTAXABILITY=" + cbs.getDED_TAXABILITY() +"&DED_DFLT_AMOUNT=" + cbs.getDED_DFLT_AMOUNT()+"'><img src =./IMG/add.png width = 90 height = 90></img></a></td>");
        out.print("</tr>");       
    }
%>
</table>
</form>
</body>
</html>