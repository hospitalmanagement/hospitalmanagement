<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<title>list of hospitals</title>
<style>
.carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 1890px;
      height:400px;
      margin: auto;
  }
.tab{
width: 1140px;
 margin: auto;
}
.container1{
 background: url(./IMG/1g.jpg) no-repeat center center fixed;
      width: 1138px;
      height:600px; 
     margin:auto;
      
}

.container2{
    margin-top: 40px;
    margin-left:2px;
    margin-bottom:50px;
}
.container3{
    margin-top: 40px;
    margin-left:2px;
    margin-bottom:50px;
}
.container4{
    margin-top: 40px;
    margin-left:2px;
    margin-bottom:50px;
}
</style>
</head>
<body >
<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="./IMG/unnamed.jpg" width="460" height="345">
      </div>

      
    
      <div class="item">
        <img src="./IMG/h1.JPG" width="460" height="345">
      </div>

      <div class="item">
        <img src="./IMG/h3.jpg" width="460" height="345">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div class="tab">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">BookMyDoc</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="HospitalManagement.html">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="LogoutController"><span class="glyphicon glyphicon-user"></span> Sign Out</a></li>
       
      </ul>
    </div>
  </div>
</nav>
</div>
<div class="container1" align="center"><br>
<br><br>
<h2>LIST OF HOSPITALS</h2><br>
<button id="myButton" class="btn btn-info btn-lg" style="height:40px ;width:300px">Global Hospitals</button>

<script type="text/javascript">
    document.getElementById("myButton").onclick = function () {
        location.href = "Global.jsp";
    };
</script>

  
<div class="container2" >
  <button id="myButton1" class="btn btn-info btn-lg" style="height:40px ;width:300px">NIMS</button>

<script type="text/javascript">
    document.getElementById("myButton1").onclick = function () {
        location.href = "Nims.jsp";
    };
</script>
     </div>

<div class="container3" >
  <button id="myButton2" class="btn btn-info btn-lg" style="height:40px ;width:300px">Medicity Hospitals</button>

<script type="text/javascript">
    document.getElementById("myButton2").onclick = function () {
        location.href = "Medicity.jsp";
    };
</script>
   </div>

<div class="container4" >
 <button id="myButton3" class="btn btn-info btn-lg" style="height:40px ;width:300px">Kamineni Hospitals</button>

<script type="text/javascript">
    document.getElementById("myButton3").onclick = function () {
        location.href = "Kamineni.jsp";
    };
</script>
   </div>
</div>
</body>
</html>